package yang.yu.java8;

/**
 * Created by yyang on 16/4/26.
 */
public enum Area {
    SHAN_XI,
    SHAN_DONG,
    GAN_SU,
    ABROAD
}
