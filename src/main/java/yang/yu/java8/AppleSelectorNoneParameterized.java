package yang.yu.java8;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by yyang on 15/8/17.
 */
public class AppleSelectorNoneParameterized {

    private Set<Apple> apples = new HashSet<Apple>();

    public void load(Apple... apples) {
        this.apples = new HashSet<Apple>(Arrays.asList(apples));
    }



    public Set<Apple> selectRed() {
        Set<Apple> results = new HashSet<>();
        for (Apple apple : apples) {
            if (apple.getColor() == Color.RED) {
                results.add(apple);
            }
        }
        return results;
    }

    public Set<Apple> selectAbroad() {
        Set<Apple> results = new HashSet<>();
        for (Apple apple : apples) {
            if (apple.getArea() == Area.ABROAD) {
                results.add(apple);
            }
        }
        return results;
    }

    public Set<Apple> selectRedAndHeavy() {
        Set<Apple> results = new HashSet<>();
        for (Apple apple : apples) {
            if (apple.getColor() == Color.RED && apple.getWeight() > 50) {
                results.add(apple);
            }
        }
        return results;
    }

    public Set<Apple> selectRedAndAbroad() {
        Set<Apple> results = new HashSet<>();
        for (Apple apple : apples) {
            if (apple.getArea() == Area.ABROAD && apple.getArea() == Area.ABROAD) {
                results.add(apple);
            }
        }
        return results;
    }
}
