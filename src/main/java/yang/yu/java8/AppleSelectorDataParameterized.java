package yang.yu.java8;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by yyang on 15/8/17.
 */
public class AppleSelectorDataParameterized {

    private Set<Apple> apples = new HashSet<Apple>();

    public void load(Apple... apples) {
        this.apples = new HashSet<Apple>(Arrays.asList(apples));
    }

    public Set<Apple> selectByColor(Color color) {
        Set<Apple> results = new HashSet<>();
        for (Apple apple : apples) {
            if (apple.getColor() == color) {
                results.add(apple);
            }
        }
        return results;
    }

    public Set<Apple> selectByColorAndWeightMoreThan(Color color, int threshold) {
        Set<Apple> results = new HashSet<>();
        for (Apple apple : apples) {
            if (apple.getColor() == color && apple.getWeight() > threshold) {
                results.add(apple);
            }
        }
        return results;
    }
}
