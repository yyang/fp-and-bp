package yang.yu.java8;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by yyang on 15/8/17.
 */
public class ItemSelector<T> {

    private Set<T> items = new HashSet<>();

    public void load(T... items) {
        this.items = new HashSet<T>(Arrays.asList(items));
    }

    public Set<T> select(ItemSelectCriteria<T> method) {
        Set<T> results = new HashSet<>();
        for (T item : items) {
            if (method.ifSatisfiedBy(item)) {
                results.add(item);
            }
        }
        return results;
    }

}
