package yang.yu.java8;

import java.util.Objects;
import java.util.UUID;

/**
 * Created by yyang on 15/8/17.
 */
public class Apple {

  private String id = UUID.randomUUID().toString();

  private Color color;

  private double weight;

  private Area area;

  public Apple(Color color, double weight, Area area) {
    this.color = color;
    this.weight = weight;
    this.area = area;
  }

  public Color getColor() {
    return color;
  }

  public double getWeight() {
    return weight;
  }

  public Area getArea() {
    return area;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Apple)) {
      return false;
    }
    Apple apple = (Apple) o;
    return Objects.equals(id, apple.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
