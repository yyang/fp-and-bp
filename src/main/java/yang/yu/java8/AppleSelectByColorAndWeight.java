package yang.yu.java8;

/**
 * Created by yyang on 15/8/18.
 */
public class AppleSelectByColorAndWeight implements AppleSelector {
    private Color color;
    private double weightThreshold;
    public AppleSelectByColorAndWeight(Color color, double weightThreshold) {
        this.color = color;
        this.weightThreshold = weightThreshold;
    }
    @Override
    public boolean isSatisfiedBy(Apple apple) {
        return apple.getColor() == color && apple.getWeight() >= weightThreshold;
    }
}
