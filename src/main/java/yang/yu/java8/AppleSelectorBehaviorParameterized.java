package yang.yu.java8;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by yyang on 15/8/17.
 */
public class AppleSelectorBehaviorParameterized {

    private Set<Apple> apples = new HashSet<Apple>();

    public void load(Apple... apples) {
        this.apples = new HashSet<Apple>(Arrays.asList(apples));
    }

    public Set<Apple> selectApple(AppleSelector method) {
        Set<Apple> results = new HashSet<>();
        for (Apple apple : apples) {
            if (method.isSatisfiedBy(apple)) {
                results.add(apple);
            }
        }
        return results;
    }

}
