package yang.yu.java8;

/**
 * Created by yyang on 16/4/27.
 */
public interface ItemSelectCriteria<T> {
    boolean ifSatisfiedBy(T item);
}
