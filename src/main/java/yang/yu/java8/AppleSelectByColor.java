package yang.yu.java8;

/**
 * Created by yyang on 15/8/18.
 */
public class AppleSelectByColor implements AppleSelector {
    private Color color;
    public AppleSelectByColor(Color color) {
        this.color = color;
    }
    @Override
    public boolean isSatisfiedBy(Apple apple) {
        return color == apple.getColor();
    }
}
