package yang.yu.java8;

import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

/**
 * Created by yyang on 15/8/17.
 */
public class BehaviorParameterizedByClassTest {

    private Apple redAndHeavy = new Apple(Color.RED, 100, Area.ABROAD);
    private Apple redAndLight = new Apple(Color.RED, 30, Area.GAN_SU);
    private Apple greenAndHeavy = new Apple(Color.GREEN, 80, Area.SHAN_DONG);
    private Apple greenAndLight = new Apple(Color.GREEN, 40, Area.GAN_SU);

    private AppleSelectorBehaviorParameterized appleSelector;

    @Before
    public void setUp() {
        appleSelector = new AppleSelectorBehaviorParameterized();
        appleSelector.load(redAndHeavy, redAndLight, greenAndLight, greenAndHeavy);
    }

    @Test
    public void selectRedApple() {
        Set<Apple> selected = appleSelector.selectApple(new AppleSelectByColor(Color.RED));
        assertThat(selected, hasItems(redAndHeavy, redAndLight));
        assertThat(selected, not(hasItems(greenAndHeavy, greenAndLight)));
    }

    @Test
    public void selectRedAndHeavyApple() {
        Set<Apple> selected = appleSelector.selectApple(new AppleSelectByColorAndWeight(Color.RED, 50));
        assertThat(selected, hasItems(redAndHeavy));
        assertThat(selected, not(hasItems(redAndLight, greenAndHeavy, greenAndLight)));
    }

}
